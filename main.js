const express = require('express');
const app = express();
const PORT = 3000;

// Baseurl: http://localhost:3000/
// Endpoint: http://localhost:3000/
app.get('/', (req, res) => {
    // req = request, res = response
    res.send('Hello World');
})

/**
 * This is an arrow function to add two numbers together.
 * @param {Number} a is the first number
 * @param {Number} b is the second number/parameter
 * @param {Number} c is the third number/parameter
 * @return {Number} This is the sum of the a, b and c params,
 */
const add = (a, b, c) => {
    const sum = a + b + c;
    return sum;
};

/**
 * This is an arrow function to add two numbers together.
 * @param {Number} a is the first number
 * @param {Number} b is the second number/parameter
 * @param {Number} c is the third number/parameter
 * @return {Number} This is the product of the a, b and c params,
 */
const mul = (a, b, c) => {
    const num = a * b * c;
    return num;
};

app.use(express.json()); // This is middleware
// Endpoint: http://localhost:3000/add
app.post('/add', (req, res) => {
    const a = req.body.a;
    const b = req.body.b;
    const c = req.body.c;
    const sum = add(a, b, c);
    res.send(sum.toString());
});

app.post('/mul', (req, res) => {
    const a = req.body.a;
    const b = req.body.b;
    const c = req.body.c;
    const num = mul(a, b, c);
    res.send(num.toString());
});

// This starts the server
app.listen(PORT, () => console.log(
    `Server listening at http://localhost:${PORT}`
));

console.log("Express application starting...");